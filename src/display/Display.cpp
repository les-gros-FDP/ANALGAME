#include "Display.h"
#include <iostream>
#include <math.h>
//#include "../core/map/Game.h"

#define DIMX 1024 // dimension in X-axis
#define DIMY 1024 // dimention in Y-axis
Display::Display() {
  // SDL initialisation
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cout << "Erreur lors de l'initialisation de la SDL : "
              << SDL_GetError() << std::endl;
    SDL_Quit();
    exit(1);
  }

  // Creation of the windows
  window =
      SDL_CreateWindow("Affichage Image", SDL_WINDOWPOS_CENTERED,
                       SDL_WINDOWPOS_CENTERED, DIMX, DIMY, SDL_WINDOW_SHOWN);
  if (window == NULL) {
    std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError()
              << std::endl;
    SDL_Quit();
    exit(1);
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

  init_Images();

  gameLogic.init(9);
}

void Display::init_Images() {
  im_Void.loadFromFile("data/hexa_pixel_blue.png", renderer);
  im_Hero.loadFromFile("data/hexa_pixel_green.png", renderer);
  im_Monster.loadFromFile("data/hexa_pixel_redish.png", renderer);
}

void Display::destroy() {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

void Display::boucle() {
  SDL_Event events;
  bool quit = false;

  // tant que ce n'est pas la fin ...
  while (!quit) {

    // tant qu'il y a des evenements � traiter (cette boucle n'est pas
    // bloquante)
    while (SDL_PollEvent(&events)) {
      if (events.type == SDL_QUIT)
        quit = true; // Si l'utilisateur a clique sur la croix de fermeture
      else if (events.type == SDL_KEYDOWN) { // Si une touche est enfoncee
        switch (events.key.keysym.scancode) {
        case SDL_SCANCODE_ESCAPE:
          quit = true;
          break;
        case SDL_SCANCODE_A: // he considere an english keyboard, as a result
                             // for us is the touch Q
          quit = true;
          break;

        default:
          break;
        }
      }
    }

    // on affiche le jeu sur le buffer caché
    draw();

    // on permute les deux buffers (cette fonction ne doit se faire qu'une seule
    // fois dans la boucle)
    SDL_RenderPresent(renderer);
  }
}

void Display::draw() {
  SDL_SetRenderDrawColor(renderer, 100, 100, 100, 255);
  SDL_RenderClear(renderer);
  drawBoardMap();
}

void Display::run() {
  boucle();
  destroy();
}

void Display::drawBoardMap() {
  int xWindow = 0;
  int yWindow = 0;
  SDL_GetWindowSize(window, &xWindow, &yWindow);
  // TODO : find a better formule for distanceScale;
  int distanceScale = 29;
  for (unsigned short i = 0; i < gameLogic.get_currentMap_NbCell(); i++) {
    Cell p = gameLogic.get_currentMap_Cell(i);
    int xHex = (int)(xWindow / 2) - distanceScale +
               distanceScale * (p.get_posX() - p.get_posY()) * cos(M_PI / 6);
    int yHex = (int)(yWindow / 2) - distanceScale +
               distanceScale * (p.get_posZ() -
                                (p.get_posX() + p.get_posY()) * sin(M_PI / 6));
    if (p.isOccupied()) { // display entities
      switch (p.get_entity_pointer()->get_name()) {
      case hero:
        im_Hero.draw(renderer, xHex, yHex, 50, 56);
        break;
      case item:
        im_Monster.draw(renderer, xHex, yHex, 50, 56);
        break;
      case atlantean:
        im_Monster.draw(renderer, xHex, yHex, 50, 56);
        break;
      case siren:
        im_Monster.draw(renderer, xHex, yHex, 50, 56);
        break;
      case nereid:
        im_Monster.draw(renderer, xHex, yHex, 50, 56);
        break;
      default:
        break;
      }
    } else { // display void
      im_Void.draw(renderer, xHex, yHex, 50, 56);
    }
  }
}