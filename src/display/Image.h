#ifndef _SDLIMAGE_H
#define _SDLIMAGE_H
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

//! \brief Pour gérer une image avec SDL2
class Image {

private:
  SDL_Surface *surface;
  SDL_Texture *texture;
  bool has_changed;

public:
  Image();
  ~Image();
  void loadFromFile(const char *filename, SDL_Renderer *renderer);
  void loadFromCurrentSurface(SDL_Renderer *renderer);
  void draw(SDL_Renderer *renderer, int x, int y, int w = -1, int h = -1);
  SDL_Texture *getTexture() const;
  void setSurface(SDL_Surface *surf);
};
#endif