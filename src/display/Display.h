#ifndef _DISPLAY_H
#define _DISPLAY_H
#include "../core/map/Game.h"
#include "Image.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

//! @brief Handle the display of the Game.
class Display {
private:
  Game gameLogic;

  SDL_Window *window;
  SDL_Renderer *renderer;

  //! @brief image représant un champs vide
  Image im_Void;
  Image im_Hero;
  Image im_Monster;
  //! @brief Detruit la fenètre SDL
  void destroy();

  //! @brief Gère la boucle d'évènement
  void boucle();

  //! @brief Met à jour le buffer
  void draw();

public:
  // CONSTRUCTOR
  //! @brief Default constructor (initialize the window, image)
  Display();

  //! @brief Initialization of all images
  void init_Images();

  //! @brief run the display
  void run();

  //! @brief draw the map on the windows
  void drawBoardMap();
};
#endif