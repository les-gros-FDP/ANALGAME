#ifndef _Position_H
#define _Position_H

//! @brief A 2D position in cubic coordinate (x+y+z=0).
class Position {
protected:
  //!  @brief Cubic coordinate component on the X-axis
  short x;
  //!  @brief Cubic coordinate component on the Y-axis
  short y;
  //!  @brief Cubic coordinate component on the Z-axis
  short z;

public:
  //  CONSTRUCTORS
  //! @brief Constructor by parameter
  //! @param x : [IN] integer value of coordinate component on the X-axis
  //! @param y : [IN] integer value of coordinate component on the Y-axis
  //! @param z : [IN] integer value of coordinate component on the z-axis
  Position(const short x, const short y, const short z);
  //! @brief Constructor by copy
  //! @param bycopy : [IN] Position to copy
  Position(const Position &bycopy);

  // ACCESSORS
  //! @brief This function return the value of the private data "x"
  short get_posX() const;
  //! @brief This function return the value of the private data "y"
  short get_posY() const;
  //! @brief This function return the value of the private data "z"
  short get_posZ() const;

  // SETTERS
  //! @brief Change the value of the private data "x" by that of the parameters
  //! @param x : [IN] integer value of coordinate component on the X-axis to
  //! change
  void set_posX(const short x);
  //! @brief Change the value of the private data "y" by that of the parameters
  //! @param y : [IN] integer value of coordinate component on the Y-axis to
  //! change
  void set_posY(const short y);
  //! @brief Change the value of the private data "z" by that of the parameters
  //! @param z : [IN] integer value of coordinate component on the Z-axis to
  //! change
  void set_posZ(const short z);

  // OTHER
  //! @brief This function return an integer. This integer represent de distance
  //! between this Position and the Position switch to parameters
  //! @param distance_to : [IN] Position to calculate distance from the current
  //! position
  unsigned short distance(const Position &distance_to);
  //! @brief This function return an booléan. It returns true if the Position
  //! switch to parameters is next to the current position
  //! @param neighboor : [IN] Position to check if the current position is next
  //! to it
  bool is_neighboor(const Position &neighboor);
};

#endif