#include "Hero.h"
#include "Character_sheet.h"
#include "Item.h"

// CONSTRUCTOR
Hero::Hero() {
  this->pos = nullptr;
  this->hpMAX = 3;
  this->healthPoint = 3;
  this->moveRange = 1;
  this->shootRange = 3;
  this->shootDamage = 1;
  this->hand2handDamage = 2;
  this->name = hero;
}

// GETTERS
unsigned short Hero::get_shootRange() { return this->shootRange; }

unsigned short Hero::get_shootDamage() { return this->shootDamage; }

unsigned short Hero::get_H2HDamage() { return this->hand2handDamage; }

// SETTERS
void Hero::set_shootRange(const unsigned short SR) { this->shootRange = SR; }

void Hero::set_shootDamage(const unsigned short SD) { this->shootDamage = SD; }

void Hero::set_H2HDamage(const unsigned short h2hD) {
  this->hand2handDamage = h2hD;
}

// OTHERS
void Hero::init(Cell *posi) { this->set_pos(posi); }

void Hero::pickup_item(Item &pickup) {
  if (this->pos->is_neighboor(pickup.get_pos())) {
    switch (pickup.get_type()) {
    case healthPotion:
      if (healthPoint < hpMAX) {
        this->healthPoint++;
      }
      break;
    case bomb:
      this->healthPoint--;
      break;
    default:;
    }
    pickup.set_isConsumed(true);
  }
}

void Hero::pickup_item(Item *pickup) {
  pickup_item(*pickup);
}