#include "Entity.h"
#include <cassert>

// CONSTRUCTORS
Entity::Entity(Cell *posi) {
  this->pos = posi;
  this->pos->set_entity_pointer(this);
}
Entity::Entity() { this->pos = nullptr; }

// ACCESSORS
Cell &Entity::get_pos() const { return *pos; }

EntityName Entity::get_name() { return this->name; }

// SETTERS
void Entity::set_name(const EntityName name) { this->name = name; }

void Entity::set_pos(Cell &posi) {
  assert(!posi.isOccupied());
  if (this->pos != nullptr) { // test if the entity is on a Cell
    // empty the ancient Cell
    this->get_pos().emptyCell();
  }
  this->pos = &posi;
  this->pos->set_entity_pointer(this);
}

void Entity::set_pos(Cell *posi) {
  this->set_pos(*posi);
}

// OTHERS