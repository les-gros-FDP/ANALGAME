#ifndef _ATLANTEAN_H
#define _ATLANTEAN_H
#include "Monster.h"

//! @brief This class respresent the monster Atlantean. It inherits from the class
//! Monster. The Atlantean gives long distance damage.
class Atlantean : public Monster {
private:
  //! @brief Range of the Atlantean's damage
  unsigned short spellRange;

public:
  //! @brief Constructor
  //! @param posi [IN/OUT] Position of the Atlantean on the map
  Atlantean(Cell *posi);

  // ACCESSORS
  //! @brief This function return the private data "spellRange"
  unsigned short get_spellRange();

  // SETTERS
  //! @brief Change the value of the private data "spellRange"
  //! @param SR [IN] Positive integer value of "spellRange" to set
  void set_spellRange(const unsigned short SR);

  // OTHERS
};

#endif