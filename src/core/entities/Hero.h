#ifndef _HERO_H
#define _HERO_H
#include "Character_sheet.h"
#include "Item.h"

//! @brief This class represent the character of the player : the Hero. It
//! inherits from the class CharacterSheet
class Hero : public CharacterSheet {
private:
  //! @brief Shooting range of the hero
  unsigned short shootRange;
  //! @brief Damage range of the hero
  unsigned short shootDamage;
  //! @brief Damage the hero can make on hand to hand combats
  unsigned short hand2handDamage;

public:
  // CONSTRUCTOR
  //! @brief Default constructor Overload
  Hero();

  // ACCESSORS
  //! @brief This function return the value of the private data shootRange
  unsigned short get_shootRange();
  //! @brief This function return the value of the private data shootDamage
  unsigned short get_shootDamage();
  //! @brief This function return the value of the private data hand2handDamage
  unsigned short get_H2HDamage();

  // SETTERS
  //! @brief Change the value of the private data shootRange by that of the
  //! parameters
  //! @param SR : [IN] Positive integer value of shootRange to change
  void set_shootRange(const unsigned short SR);
  //! @brief Change the value of the private data shootDamage by that of the
  //! parameters
  //! @param SD : [IN] Positive integer value of shootDamage to change
  void set_shootDamage(const unsigned short SD);
  //! @brief Change the value of the private data hand2handDamage by that of the
  //! parameters
  //! @param h2hD : [IN] Positive integer value of hand2handDamage to change
  void set_H2HDamage(const unsigned short h2hD);

  // OTHERS
  //! @brief Initialise values of private data
  //! @param posi : [IN/OUT] position on the map of the hero to set
  void init(Cell *posi);
  //! @brief Implement the iteraction with the item pickup if it can be picked
  //! up
  //! @param pickup : [IN/OUT] adress of the item to interact with
  void pickup_item(Item &pickup);
  //! @brief Implement the iteraction with the item pickup if it can be picked
  //! up
  //! @param pickup : [IN/OUT] pointer on the item to interact with
  void pickup_item(Item *pickup);
};

#endif