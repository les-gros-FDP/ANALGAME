#ifndef _ENTITY_H
#define _ENTITY_H
#include "../Cell.h"

//! @brief Types of Entities existing in the game
enum EntityName { hero, item, atlantean, siren, nereid };

//! @brief This class represent any Entity in the game.
class Entity {
protected:
  //! @brief Type of the entity
  enum EntityName name;
  //! @brief Cell on the map on which the Entity is
  Cell *pos;

public:
  // CONSTRUCTORS
  //! @brief Constructor by parameter
  //! @param pos [IN/OUT] Position of the entity on the map
  Entity(Cell *pos);
  //! @brief Constructor by default
  Entity();

  // ACCESSORS
  //! @brief This function return the private data "name"
  EntityName get_name();
  //! @brief This function return the adress of the private data "pos"
  Cell &get_pos() const;

  // SETTERS
  //! @brief Change the value of the private data "name" by that of the
  //! parameters
  //! @param name [IN] Type of the entity to set
  void set_name(const EntityName name);
  //! @brief Change the value of the private data "pos" by that of the
  //! parameters
  //! @param pos [IN/OUT] Adress of the position to set
  void set_pos(Cell &posi);
  //! @brief Change the value of the private data "pos" by that of the
  //! parameters type pointer
  //! @param pos [IN/OUT] Pointer on the position to set
  void set_pos(Cell *posi);

  // OTHERS
};

#endif