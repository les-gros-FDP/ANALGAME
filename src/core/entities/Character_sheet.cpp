#include "Character_sheet.h"
#include "Entity.h"
#include <cassert>

// CONSTRUCTOR
CharacterSheet::CharacterSheet(const unsigned short hpMAX,
                               const unsigned short healthPoint,
                               const unsigned short moveRange, Cell *posi)
    : Entity(posi) {
  this->hpMAX = hpMAX;
  this->healthPoint = healthPoint;
  this->moveRange = moveRange;
}
CharacterSheet::CharacterSheet() {}
// DESTRUCTOR
CharacterSheet::~CharacterSheet() {}

// ACCESSORS
unsigned short CharacterSheet::get_hpMAX() const { return this->hpMAX; }

unsigned short CharacterSheet::get_HP() const { return this->healthPoint; }

unsigned short CharacterSheet::get_MR() const { return this->moveRange; }

// SETTERS
void CharacterSheet::set_hpMAX(const unsigned short hpM) { this->hpMAX = hpM; }

void CharacterSheet::set_HP(const unsigned short hp) { this->healthPoint = hp; }

void CharacterSheet::set_MR(const unsigned short mr) { this->moveRange = mr; }

// OTHERS
void CharacterSheet::takeDamage(const unsigned short damage) {
  int tmp = healthPoint - damage;
  if (tmp > 0) {
    set_HP(tmp);
  } else {
    set_HP(0);
  }
}

void CharacterSheet::healing(const unsigned short heal_amount) {
  int tmp = healthPoint + heal_amount;
  if (tmp > hpMAX) {
    this->set_HP(hpMAX);
  } else {
    this->set_HP(tmp);
  }
}

void CharacterSheet::moveToward(Cell &destination) {
  if (this->pos->distance(destination) <= get_MR()) {
    // set the new Cell
    this->set_pos(destination);
  }
}

// NEED TO BE TESTED
// std::vector<Cell> CharacterSheet::show_MR() {
//   std::vector<Cell> MR_pos;
//   for (int x = -this->moveRange; x <= this->moveRange; x++) {
//     for (int y = std::max(-this->moveRange, -x - this->moveRange);
//          y <= std::min(+this->moveRange, -x + this->moveRange); y++) {
//       int z = -x - y;
//       MR_pos.push_back(Cell(x, y, z));
//     }
//   }
//   return MR_pos;
// }