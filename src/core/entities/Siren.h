#ifndef _SIREN_H
#define _SIREN_H
#include "Monster.h"

//! @brief This class respresent the monster Siren. It inherits from the class
//! Monster. Sirens gives hand to hand damage.
class Siren : public Monster {
private:
  //! @brief Damage the Siren can do to the hero
  unsigned short hand2handDamage;

public:
  //! @brief Constructor
  //! @param posi [IN/OUT] Position of the siren on the map
  Siren(Cell *posi);

  // ACCESSORS
  //! @brief Returns the value of the private data "hand2handDamage"
  unsigned short get_H2HDamage() const;

  // SETTERS
  //! @brief Change the value of the private data "hand2handDamage" by that of
  //! the parameter
  //! @param h2hD [IN] positive integer value of "hand2handDamage" to change
  void set_H2HDamage(const unsigned short h2hD);

  // OTHERS
};

#endif