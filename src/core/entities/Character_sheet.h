#ifndef _CHARACTER_SHEET_H
#define _CHARACTER_SHEET_H

#include "Entity.h"
#include <string>
#include <vector>

//! @brief This class represents a character. It inherits from the class Entity.
class CharacterSheet : public Entity {
protected:
  //! @brief Maximum of Health a character can have
  unsigned short hpMAX;
  //! @brief Number of health point remaining
  unsigned short healthPoint;
  //! @brief Movement range of the character
  unsigned short moveRange;

public:
  // CONSTRUCTOR
  //! @brief Constructor by default
  CharacterSheet();
  //! @brief Constructor by parameter
  //! @param hpMAX [IN] Positive integer value of the private data hpMAX
  //! @param healthPoint [IN] Positive integer value of the private data
  //! healthPoint
  //! @param moveRange [IN] Positive integer value of the private data moveRange
  //! @param posi [IN/OUT] Position on the map of the character
  CharacterSheet(const unsigned short hpMAX, const unsigned short healthPoint,
                 const unsigned short moveRange, Cell *posi);

  // DESTRUCTOR
  //! @brief Destructor
  ~CharacterSheet();

  // ACCESSORS
  //! @brief This function return the value of the private data "hpMAX"
  unsigned short get_hpMAX() const;
  //! @brief This function return the value of the private data "healthPoint"
  unsigned short get_HP() const;
  //! @brief This function return the value of the private data "moveRange"
  unsigned short get_MR() const;

  // SETTERS
  //! @brief Change the value of the private data "hpMAX" by that of the
  //! parameters
  //! @param hpM [IN] Positive integer value of "hpMAX" to set
  void set_hpMAX(const unsigned short hpM);
  //! @brief Change the value of the private data "healthPoint" by that of the
  //! parameters
  //! @param hp [IN] Positive integer value of "healthPoint" to set
  void set_HP(const unsigned short hp);
  //! @brief Change the value of the private data "moveRange" by that of the
  //! parameters
  //! @param mr [IN] Positive integer value of "moveRange" to set
  void set_MR(const unsigned short mr);

  // OTHERS
  //! @brief Damage the character
  //! @param damage [IN] Positive integer number of heathpoints that the
  //! character loses
  void takeDamage(const unsigned short damage);
  //! @brief Heal the character
  //! @param healt_amount [IN] Positive integer number of healthPoints that the
  //! character gains
  void healing(const unsigned short heal_amount);
  //! @brief Deplace the Character correctly
  //! @param destination [IN/OUT] Cell destination of the character
  void moveToward(Cell &destination);
  //! @brief Return a vector of Cell in range of movement
  // std::vector<Cell> show_MR();
};

#endif
