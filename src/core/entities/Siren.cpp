#include "Siren.h"
#include "Entity.h"
#include "Monster.h"

// CONSTRUCTOR
Siren::Siren(Cell *posi) : Monster(posi) { this->set_name(siren); }

// GETTER
unsigned short Siren::get_H2HDamage() const { return this->hand2handDamage; }

// SETTER
void Siren::set_H2HDamage(const unsigned short h2hD) {
  this->hand2handDamage = h2hD;
}