#ifndef _NEREID_H
#define _NEREID_H
#include "Monster.h"

//! @brief This class represents the monster Nereid. It inherits from the the
//! class Monster. Nereids gives long disance damage. Nereids can make appear
//! new obstacles in the way, and attack only in a specific range.
class Nereid : public Monster {
private:
  //! @brief Range of the Nereid's damage
  unsigned short spellRange;

public:
  //! @brief Constructor
  //! @param posi [IN/OUT] Position on the map
  Nereid(Cell *posi);

  // ACCESSORS
  //! @brief This function return the private data "spellRange"
  unsigned short get_spellRange();

  // SETTERS
  //! @brief Change the value of the private data "spellRange"
  //! @param SR [IN] Positive integer value of "spellRange" to set
  void set_spellRange(const unsigned short SR);

  // OTHERS
};

#endif