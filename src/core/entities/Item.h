#ifndef _ITEM_H
#define _ITEM_H
#include "../Cell.h"
#include "Entity.h"

//! @brief Types of items existing in the game
enum typeItem { healthPotion, coral, bomb };

//! @brief This class represents an item. It inherits from the class Entity.
class Item : public Entity {
private:
  //! @brief Define if the item is pickable by the hero
  bool isPickable;
  //! @brief Indicate if the item have been consumed yet
  bool isConsumed;
  //! @brief Define the type of the item
  typeItem type;

public:
  // CONSTRUCTORS
  //! @brief Constructor by parameter
  //! @param posi [IN/OUT] Pointer on cell, position of the item
  //! @param typei [IN] Type of the item
  Item(Cell *posi, typeItem typei);
  //! @brief Constructor by copy
  //! @param copy [IN] Item to copy
  Item(const Item &copy);

  // GETTERS
  //! @brief This function return the value of private data member isPickable
  bool get_isPickable() const;
  //! @brief This function return the value of private data member isConsumed
  bool get_isConsumed() const;
  //! @brief This function return the value of private data member type
  typeItem get_type() const;

  // SETTERS
  //! @brief Change the value of private data member isConsumed
  //! @param ic [IN] Booléan, new value of isConsumed
  void set_isConsumed(bool ic);
};

#endif