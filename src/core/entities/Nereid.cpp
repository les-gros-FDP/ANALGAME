#include "Nereid.h"
#include "Monster.h"

// CONSTRUCTOR
Nereid::Nereid(Cell *posi) : Monster(posi) { this->set_name(nereid); }

// ACCESSORS
unsigned short Nereid::get_spellRange() { return this->spellRange; }

// SETTERS
void Nereid::set_spellRange(const unsigned short SR) { this->spellRange = SR; }

// OTHERS