#include "Item.h"
#include "Entity.h"
#include <cassert>

// CONSTRUCTORS
Item::Item(Cell *posi, typeItem typei) : Entity(posi) {
  this->type = typei;
  this->isConsumed = false;
  if (this->type == bomb) {
    this->isPickable = false;
  } else {
    this->isPickable = true;
  }
  this->name = item;
}

Item::Item(const Item &copy) {
  assert(copy.isConsumed == false);
  this->pos = copy.pos;
  this->name = copy.name;
  this->isPickable = copy.isPickable;
  this->isConsumed = copy.isConsumed;
  this->type = copy.type;
}

// GETTERS
bool Item::get_isPickable() const { return this->isPickable; }

bool Item::get_isConsumed() const { return this->isConsumed; }

typeItem Item::get_type() const { return this->type; }

// SETTERS
void Item::set_isConsumed(bool ic) { this->isConsumed = ic; }
