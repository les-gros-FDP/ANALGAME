#ifndef _MONSTER_H
#define _MONSTER_H
#include "Character_sheet.h"

//! This class represents a Monster. It inherits from CharacterSheet
class Monster : public CharacterSheet {
protected:
public:
  //! @brief Default constructor Overload
  //! @param posi [IN/OUT] Position of the monster the map
  Monster(Cell *posi);
};

#endif