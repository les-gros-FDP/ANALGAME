#include "Atlantean.h"
#include "Entity.h"
#include "Monster.h"
Atlantean::Atlantean(Cell *posi) : Monster(posi) { this->set_name(atlantean); }

// ACCESSORS
unsigned short Atlantean::get_spellRange() { return this->get_spellRange(); }

// SETTERS
void Atlantean::set_spellRange(const unsigned short SR) {
  this->spellRange = SR;
}

// OTHERS
