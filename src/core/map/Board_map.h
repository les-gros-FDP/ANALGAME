#ifndef _BOARD_MAP_H
#define _BOARD_MAP_H
#include "../Position.h"
#include "../entities/Hero.h"
#include "../entities/Item.h"
#include "../entities/Monster.h"
#include <vector>

//! @brief This represent the Board where you play.
class BoardMap : public Position {
private:
  //! @brief Level reached
  unsigned short level;
  //! @brief the Hero played
  Hero *player;
  //! @brief Dynamic table of monsters
  std::vector<Monster> tabMonster;
  //! @brief Dynamic table of items
  std::vector<Item> tabItem;
  //! @brief Pointer toward a table of Cell that are currently in the board.
  Cell *arrayBoard;
  //! @brief The Rayon of the Board.
  unsigned short rayon;
  //! @brief The number of case in the table of Cell.
  unsigned short sizeArrayBoard;

public:
  // CONSTRUCTORS
  //! @brief Constructor by parameter.
  BoardMap(short x, short y, short z);

  //! @brief Default constructor. Only used to create table of self.
  BoardMap();

  // DESTRUCTOR
  //! @brief Destructor
  ~BoardMap(); // NOTE : TODO : don't forget to delete freed memory

  // ACCESSORS
  //! @brief This function return the value of the private data "level"
  unsigned short get_level() const;

  //! @brief return the number of Cell in the board
  unsigned short get_nbCell() const;

  // TODO : something
  Monster &get_const_Monster() const;
  Monster &get_Monster();

  //! @brief Return the number of cell in the Cell's table.
  unsigned short get_sizeArrayBoard();

  //! @brief This function returns the array of Cell board. It allow to make
  //! changes on the array.
  Cell &get_nc_arrayBoard();

  //! @brief This function returns the array of Cell board. It doesn't allow
  //! to make changes on the array.
  const Cell &get_arrayBoard() const;

  //! @brief This function returns a Cell on the board by its index. It
  //! allow to make changes on the Cell.
  Cell &get_nc_Cell_on_board(unsigned int i);

  //! @brief This function returns a Cell on the board by its index. It
  //! doesn't allow to make changes on the Cell.
  const Cell &get_Cell_on_board(unsigned int i) const;

  // SETTERS
  //! @brief Change the value of the private data "level" by that of the
  //! parameters
  void set_level(const unsigned short lvl);

  // OTHERS

  // TODO : Those function create new entity and set all the dynamic table
  // accordingly (Cell.isOccupied set to true, etc etc..)
  void invoke_monster(); // this function may need an enum variable to determine
                         // which kind of monster is summoned
  void invoke_item();

  //! @brief This function init the BoardMap with the given parameter
  void init(Hero *hero, unsigned short lvl, int size);

  //! @brief Calculate the supposed index of the Cell according to the size of
  //! the the arrayBoard.
  //! @param x : x coordonate.
  //! @param y : y coordonate.
  //! @param z : z coordonate.
  unsigned short convertCoordToIndex(short x, short y, short z);
  //! @brief Calculate the supposed index of the Cell according to the size of
  //! the the arrayBoard.
  //! @param coord : The Cell which we want the index.
  unsigned short convertCoordToIndex(const Cell &coord);
};

#endif