#include "Board_map.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>

BoardMap::BoardMap(short x, short y, short z) : Position(x, y, z) {
  this->level = 0;
  this->player = nullptr;
  this->arrayBoard = nullptr;
  this->rayon = 0;
  this->sizeArrayBoard = 0;
}

BoardMap::BoardMap() : BoardMap(0, 0, 0) {}

BoardMap::~BoardMap() { delete[] arrayBoard; }

Cell &BoardMap::get_nc_arrayBoard() { return *arrayBoard; }

const Cell &BoardMap::get_arrayBoard() const { return *arrayBoard; }

unsigned short BoardMap::get_sizeArrayBoard() { return this->sizeArrayBoard; }

Cell &BoardMap::get_nc_Cell_on_board(unsigned int i) {
  return this->arrayBoard[i];
}

const Cell &BoardMap::get_Cell_on_board(unsigned int i) const {
  return this->arrayBoard[i];
}

void BoardMap::init(Hero *hero, unsigned short lvl, int size = 9) {
  this->player = hero;

  // radius of the hexagon created
  this->rayon = size;

  this->sizeArrayBoard = (1 + ((size * (size + 1)) / 2) * 6);

  // creating a table with the correct size
  arrayBoard = new Cell[this->sizeArrayBoard];
  // this setup the array board with only possible coordinate x,y,z as :
  // x+y+z=0 into a shape of an hexagon
  int i = 0;
  for (int x = -size; x <= size; x++) {
    for (int y = std::max(-size, -x - size); y <= std::min(+size, -x + size);
         y++) {
      int z = -x - y;
      arrayBoard[i] = (Cell(x, y, z));
      i++;
    }
  }
  this->level = lvl;
}

unsigned short BoardMap::get_nbCell() const { return this->sizeArrayBoard; }

unsigned short BoardMap::convertCoordToIndex(short x, short y, short z) {
  assert(x + y + z == 0);
  return ((x + this->rayon + 1) * (2 * this->rayon + 1) -
          (((1.0 / 2) * this->rayon) * (this->rayon + 1)) -
          ((1.0 / 2)) * std::abs(x) * (x + 1) + y + this->rayon + fmax(0, x)) -
         (2 * this->rayon + 1);
}
unsigned short BoardMap::convertCoordToIndex(const Cell &coord) {
  return convertCoordToIndex(coord.get_posX(), coord.get_posY(),
                             coord.get_posZ());
}
