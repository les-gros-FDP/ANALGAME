#include "Game.h"
#include "Board_map.h"
#include <cstdio>

Game::Game() : player() {
  this->rayon = 0;
  this->currentBoardMapIndex = 1;
  this->sizeArrayMap = 0;
}

Game::~Game() { delete[] arrayMap; }

unsigned short Game::get_currentMap_NbCell() const {
  return currentBoardMap->get_nbCell();
}

const Cell &Game::get_currentMap_Cell(unsigned short const i) const {
  return currentBoardMap->get_Cell_on_board(i);
}

void Game::init(unsigned short size = 9) {

  // radius of the hexagon created
  this->rayon = size;
  // creating a table with the correct size
  this->sizeArrayMap = (1 + ((size * (size + 1)) / 2) * 6);
  arrayMap = new BoardMap[this->sizeArrayMap];
  // this setup the array board with only possible coordinate x,y,z as :
  // x+y+z=0 into a shape of an hexagon
  int i = 0;
  for (int x = -size; x <= size; x++) {
    for (int y = std::max(-size, -x - size); y <= std::min(+size, -x + size);
         y++) {
      int z = -x - y;
      arrayMap[i].set_posX(x);
      arrayMap[i].set_posY(y);
      arrayMap[i].set_posZ(z);
      i++;
    }
  }

  // TESTING : NEED TO BE IMPLEMENTED CORRECTLY
  this->currentBoardMapIndex = (this->sizeArrayMap / 2);
  this->currentBoardMap = &arrayMap[this->sizeArrayMap / 2];
  this->currentBoardMap->init(&player, 0, 9);
  this->player.init(&this->currentBoardMap->get_nc_Cell_on_board(100));
}