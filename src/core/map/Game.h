#ifndef _GAME_H
#define _GAME_H
#include "../entities/Hero.h"
#include "Board_map.h"

//! @brief Contain all the game logic.
class Game {
private:
  //! @brief Pointer on a table of BoardMap.
  BoardMap *arrayMap;
  //! @brief Rayon of the map.
  unsigned short rayon;
  //! @brief Size of the array of BoardMap.
  unsigned short sizeArrayMap;

  //! @brief Pointer toward the BoardMap currently in use (the one where the
  //! Hero stand).
  BoardMap *currentBoardMap;
  //! @brief The index of the BoardMap currently in use (the one where the Hero
  //! stand).
  unsigned int currentBoardMapIndex;

  //! @brief The player's Hero.
  Hero player;

public:
  // CONSTRUCTORS
  //! @brief Default constructor
  Game();
  // DESTRUCTORS
  //! @brief Destructor
  ~Game();

  //! @brief return the number of Cell in the current BoardMap
  unsigned short get_currentMap_NbCell() const;

  //! @brief return the wanted Cell
  //! @param i : represent the ième Cell of the current board map
  const Cell &get_currentMap_Cell(unsigned short const i) const;

  //! @brief Init the Game.
  //! @param size : The size wanted of the map (the rayon).
  void init(unsigned short size);
};

#endif