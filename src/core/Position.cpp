#include "Position.h"
#include <cassert>
#include <cmath>

// CONSTRUCTORS

Position::Position(const short x, const short y, const short z) {
  assert(x + y + z == 0);
  this->x = x;
  this->y = y;
  this->z = z;
}

Position::Position(const Position &bycopy) {
  assert(bycopy.x + bycopy.y + bycopy.z == 0);
  this->x = bycopy.x;
  this->y = bycopy.y;
  this->z = bycopy.z;
}

// ACCESSORS
short Position::get_posX() const { return this->x; }
short Position::get_posY() const { return this->y; }
short Position::get_posZ() const { return this->z; }

// SETTERS
void Position::set_posX(const short x) { this->x = x; }
void Position::set_posY(const short y) { this->y = y; }
void Position::set_posZ(const short z) { this->y = z; }

// OTHERS
unsigned short Position::distance(const Position &distance_to) {
  return ((abs(this->get_posX() - distance_to.get_posX()) +
           abs(this->get_posY() - distance_to.get_posY()) +
           abs(this->get_posZ() - distance_to.get_posZ())) /
          2);
}

bool Position::is_neighboor(const Position &neighboor) {
  // Return true if neighboor is next to the Position
  return (this->distance(neighboor) == 1);
}