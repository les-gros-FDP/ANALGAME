#include "Cell.h"
#include <cassert>
#include <cmath>

// CONSTRUCTORS

Cell::Cell(const short x, const short y, const short z) : Position(x,y,z) {
  this->entity = nullptr;
}

Cell::Cell(const Cell &bycopy) : Position(bycopy) {
  this->entity = bycopy.entity;
}

Cell::Cell() : Position (0,0,0) {
  this->entity=nullptr;
}

// ACCESSORS
Entity *Cell::get_entity_pointer() const { return this->entity; }

// SETTERS
void Cell::set_entity_pointer(Entity *entity_pointer) {
  this->entity = entity_pointer;
}

// OTHERS
bool Cell::isOccupied() const {
  // Return true if ocupied
  return (this->entity != nullptr);
}

void Cell::emptyCell() { this->entity = nullptr; }