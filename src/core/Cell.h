#ifndef _Cell_H
#define _Cell_H
#include "./Position.h"

//! @brief Forward declaration of Entity.
class Entity;

//! @brief The Cell of the Board. Inherits from Position.
class Cell : public Position {
private:
  //! @brief Pointer toward an entity
  Entity *entity;

public:
  //  CONSTRUCTORS
  //! @brief Constructor by parameter
  Cell(const short x, const short y, const short z);
  //! @brief Constructor by copy
  Cell(const Cell &bycopy);

  Cell();

  // ACCESSORS
  //! @brief This function return the pointer toward the entity which is on
  //! that Cell
  Entity *get_entity_pointer() const;

  // SETTERS
  //! @brief This function set the void pointer toward the entity which is on
  //! that Cell
  void set_entity_pointer(Entity *entity_pointer);

  // OTHER
  //! @brief This function return true if the Cell is occupied by any entity
  bool isOccupied() const;
  //! @brief This procedure remove pointer and type to "free" the Cell
  void emptyCell();
};

#endif