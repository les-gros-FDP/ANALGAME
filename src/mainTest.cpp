#include "core/Cell.h"
#include "core/entities/Atlantean.h"
#include "core/entities/Character_sheet.h"
#include "core/entities/Hero.h"
#include "core/entities/Item.h"
#include "core/entities/Monster.h"
#include "core/entities/Nereid.h"
#include "core/entities/Siren.h"
#include "core/map/Board_map.h"
#include "display/Display.h"

#include <cassert>
#include <iostream>

using namespace std;

void test_Position() {
  cout << "Beginning of test for Position class... ";

  Position test1(1, 1, -2);
  Position test2(-1, 1, 0);
  Position test3(1, -4, 3);
  assert((test1.get_posX() == 1) && (test1.get_posY() == 1) &&
         (test1.get_posZ() == -2));
  assert((test2.get_posX() == -1) && (test2.get_posY() == 1) &&
         (test2.get_posZ() == 0));
  assert((test3.get_posX() == 1) && (test3.get_posY() == -4) &&
         (test3.get_posZ() == 3));

  // test setters
  test1.set_posX(4);
  test1.set_posY(-2);
  test1.set_posZ(-2);
  assert((test1.get_posX() == 4) && (test1.get_posY() == -2) &&
         (test1.get_posZ() == -2));

  // test constructor by copy
  Position testCopie(test1);
  assert((testCopie.get_posX() == test1.get_posX()) &&
         (testCopie.get_posY() == test1.get_posY()) &&
         (testCopie.get_posZ() == testCopie.get_posZ()));

  // test distance
  unsigned short dist, distWished;
  dist = test2.distance(test3);
  distWished = ((abs(test2.get_posX() - test3.get_posX()) +
                 abs(test2.get_posY() - test3.get_posY()) +
                 abs(test2.get_posZ() - test3.get_posZ())) /
                2);
  assert(dist == distWished);

  // test is_neighboor
  Position testNeighboor(0, 0, 0);
  assert(test2.is_neighboor(testNeighboor));
  assert(!(test1.is_neighboor(test2)));

  cout << "OK!" << endl;
}

void test_Cell() {
  cout << "Beginning of test for Cell class... ";

  // Test constructor by parameter, assert are in constructor
  Cell test1(1, 1, -2);
  Cell test2(-1, 1, 0);
  Cell test3(1, -4, 3);

  // test get_entity_pointer
  assert(test1.get_entity_pointer() == nullptr);
  assert(test2.get_entity_pointer() == nullptr);
  assert(test3.get_entity_pointer() == nullptr);

  // test constructor by copy
  Cell testCopie(test1);
  assert(testCopie.get_entity_pointer() == test1.get_entity_pointer());

  // test isOccupied
  assert(!test1.isOccupied());
  assert(!test2.isOccupied());
  assert(!test3.isOccupied());

  // test set_entity_pointer
  Entity test_entity(&test1);
  assert(test1.isOccupied());
  assert(test1.get_entity_pointer() == &test_entity);
  test2.set_entity_pointer(&test_entity);
  assert(test2.isOccupied());
  assert(test2.get_entity_pointer() == &test_entity);

  // test emptyCell
  test1.emptyCell();
  test2.emptyCell();
  assert(test1.get_entity_pointer() == nullptr);
  assert(test2.get_entity_pointer() == nullptr);

  cout << "OK!" << endl;
}

void test_Entity() {
  cout << "Beginning of test for Entity class... ";

  // Test constructor
  Cell *cellPtr = new Cell(0, 1, -1);
  Entity test(cellPtr);
  assert(&test.get_pos() == cellPtr);
  assert(cellPtr->get_entity_pointer() == &test);

  // Test setters and getters
  EntityName name = siren;
  Cell cellRef(5, -3, -2);

  test.set_name(name);
  assert(test.get_name() == name);

  test.set_pos(cellRef);
  assert(&test.get_pos() == &cellRef);
  assert(cellRef.isOccupied());
  assert(cellRef.get_entity_pointer() == &test);
  assert(!cellPtr->isOccupied());

  test.set_pos(cellPtr);
  assert(&test.get_pos() == cellPtr);
  assert(cellPtr->isOccupied());
  assert(cellPtr->get_entity_pointer() == &test);
  assert(!cellRef.isOccupied());

  delete cellPtr;

  cout << "OK!" << endl;
}

void test_Item() {
  cout << "Beginning of test for Item class... ";

  // Test constructor by parameter
  Cell test1(1, -2, 1);
  typeItem type1 = healthPotion;
  Item testItem1(&test1, type1);
  assert(testItem1.get_type() == type1);
  assert(testItem1.get_isPickable());
  assert(!(testItem1.get_isConsumed()));

  Cell test2(1, 0, -1);
  typeItem type2 = bomb;
  Item testItem2(&test2, type2);
  assert(testItem2.get_type() == type2);
  assert(!(testItem2.get_isPickable()));
  assert(!(testItem2.get_isConsumed()));

  // Test constructor by copy
  Item testItem1Copy(testItem1);
  assert(testItem1Copy.get_type() == testItem1.get_type());
  assert(testItem1Copy.get_isPickable() == testItem1.get_isPickable());
  assert(testItem1Copy.get_isConsumed() == testItem1.get_isConsumed());

  // Test setter
  testItem1.set_isConsumed(true);
  assert(testItem1.get_isConsumed());

  cout << "OK!" << endl;
}

void test_CharacterSheet() {
  cout << "Beginning of test for CharacterSheet class... ";
  Cell test_cell(0, 0, 0);

  // Test constructor
  CharacterSheet test(3, 3, 1, &test_cell);

  // Test accessors
  assert(test_cell.get_entity_pointer() == &test);
  assert((test.get_HP() == 3) && (test.get_MR() == 1) &&
         (test.get_hpMAX() == 3));

  // Test setters
  test.set_hpMAX(4);
  assert(test.get_hpMAX() == 4);
  test.set_HP(4);
  assert(test.get_HP() == 4);
  test.set_MR(2);
  assert(test.get_MR() == 2);

  // Test others
  test.takeDamage(2);
  assert(test.get_HP() == 2);
  test.healing(3);
  assert(test.get_HP() == 4);

  Cell test_deplacement(1, 0, -1);
  test.moveToward(test_deplacement);
  assert(test_deplacement.isOccupied() == true);
  assert(test_deplacement.get_entity_pointer() == &test);

  cout << "OK!" << endl;
}

void test_Monster() {
  cout << "Beginning of test for Monster class... ";
  Cell test_cell(0, 0, 0);
  // Test constructor
  Monster test(&test_cell);
  assert(test_cell.get_entity_pointer() == &test);
  assert(test.get_HP() == 1);
  assert(test.get_hpMAX() == 1);
  assert(test.get_MR() == 1);

  cout << "OK!" << endl;
}

void test_Atlantean() {
  cout << "Beginning of test for Atlantean class... ";
  // TEST HERE
  cout << "OK!" << endl;
}

void test_Nereid() {
  cout << "Beginning of test for Nereid class... ";
  // TEST HERE
  cout << "OK!" << endl;
}

void test_Siren() {
  cout << "Beginning of test for Siren class... ";
  // TEST HERE
  cout << "OK!" << endl;
}

void test_Hero() {
  cout << "Beginning of test for Hero class... ";

  Cell test(0, 0, 0);
  // Test constructor
  Hero test_hero;
  // Test init
  test_hero.init(&test);
  assert(test.get_entity_pointer() == &test_hero);

  // Test accessors
  assert(test_hero.get_MR() == 1);
  assert(test_hero.get_hpMAX() == 3);
  assert(test_hero.get_HP() == 3);
  assert(test_hero.get_shootRange() == 3);
  assert(test_hero.get_shootDamage() == 1);
  assert(test_hero.get_H2HDamage() == 2);

  // Test setters
  test_hero.set_MR(2);
  test_hero.set_hpMAX(4);
  test_hero.set_HP(4);
  test_hero.set_shootRange(4);
  test_hero.set_shootDamage(2);
  test_hero.set_H2HDamage(3);

  assert(test_hero.get_MR() == 2);
  assert(test_hero.get_hpMAX() == 4);
  assert(test_hero.get_HP() == 4);
  assert(test_hero.get_shootRange() == 4);
  assert(test_hero.get_shootDamage() == 2);
  assert(test_hero.get_H2HDamage() == 3);

  // Test pickup item
  Cell * cell1 = new Cell(1,-1,0);
  Cell * cell2 = new Cell(-1,1,0);
  Cell * cell3 = new Cell(0,1,-1);
  Cell * cell4 = new Cell(5,5,-10);
  Item * healthPotionPtr = new Item(cell1, healthPotion);
  Item bombRef(cell2, bomb);
  Item healthPotionRef(cell3, healthPotion);
  Item bombDistant(cell4, bomb);

  test_hero.pickup_item(healthPotionRef);
  assert(test_hero.get_HP() == 4);
  assert(healthPotionRef.get_isConsumed());

  test_hero.pickup_item(bombRef);
  assert(test_hero.get_HP() == 3);
  assert(bombRef.get_isConsumed());

  test_hero.pickup_item(healthPotionPtr);
  assert(test_hero.get_HP() == 4);
  assert(healthPotionPtr->get_isConsumed());

  test_hero.pickup_item(bombDistant);
  assert(test_hero.get_HP() == 4);
  assert(!bombDistant.get_isConsumed());

  delete cell1;
  delete cell2;
  delete cell3;
  delete cell4;
  delete healthPotionPtr;
  cout << "OK!" << endl;
}

void test_BoardMap() {
  cout << "Beginning of test for BoardMap class... ";

  BoardMap test;
  Hero hero;
  test.init(&hero, 0, 20);

  // test convertion coord to index
  for (ushort i = 0; i < test.get_sizeArrayBoard(); i++) {
    Cell *ptr_cell = &test.get_nc_Cell_on_board(i);
    assert(test.convertCoordToIndex(*ptr_cell) == i);
  }

  cout << "OK!" << endl;
}

void test_Game() {
  cout << "Beginning of test for Game class... ";
  // TEST HERE
  cout << "OK!" << endl;
}

void test_Display() {
  cout << "Beginning of test for Display class... ";

  Display game;
  game.run();

  cout << "OK!" << endl;
}

// MAIN
int main(void) {
  test_Position();
  test_Cell();
  test_Entity();
  test_Item();
  test_CharacterSheet();
  test_Monster();
  test_Hero();
  test_BoardMap();
  test_Display();
  return 0;
}