CC = g++
CFLAGS = -Wall -ggdb -c
SDLFLAGS = -lSDL2 -lSDL2_image
INCLUDE_DIR_SDL = -I/usr/include/SDL2

SRC_DIR = ./src/
BIN_DIR = ./bin/
OBJ_DIR = ./obj/
CORE_DIR = $(SRC_DIR)core
DISP_DIR = $(SRC_DIR)display/

OBJ_FILES = obj/Board_map.o obj/Hero.o obj/Atlantean.o obj/Nereid.o obj/Siren.o obj/Monster.o obj/Character_sheet.o obj/Item.o obj/Cell.o obj/mainTest.o obj/Game.o obj/Display.o obj/Entity.o obj/Image.o obj/Position.o

.PHONY: all
all: $(BIN_DIR)test

################################### TEST ######################################
$(BIN_DIR)test: $(OBJ_FILES)
	$(CC) $^ -o $@ $(SDLFLAGS) 

obj/mainTest.o: src/mainTest.cpp $(CORE_DIR)/map/Board_map.h $(CORE_DIR)/entities/Nereid.h $(CORE_DIR)/entities/Atlantean.h $(CORE_DIR)/entities/Siren.h $(CORE_DIR)/entities/Monster.h $(CORE_DIR)/entities/Hero.h $(CORE_DIR)/entities/Character_sheet.h $(CORE_DIR)/entities/Item.h $(CORE_DIR)/Cell.h $(CORE_DIR)/map/Game.h $(DISP_DIR)Display.h $(DISP_DIR)Image.h $(CORE_DIR)/entities/Entity.h $(CORE_DIR)/Position.h
	$(CC) $(CFLAGS)  $(INCLUDE_DIR_SDL) src/mainTest.cpp -o $@ 


###############################################################################
############################### DEPENDENCIES ##################################
###############################################################################

################################## ENTITIES ###################################
obj/Entity.o: $(CORE_DIR)/entities/Entity.cpp $(CORE_DIR)/entities/Entity.h $(CORE_DIR)/Cell.h
	$(CC) $(CFLAGS)  $< -o $@

obj/Character_sheet.o: $(CORE_DIR)/entities/Character_sheet.cpp $(CORE_DIR)/entities/Character_sheet.h $(CORE_DIR)/entities/Entity.h
	$(CC) $(CFLAGS) $< -o $@

###### Monster ######
obj/Monster.o: $(CORE_DIR)/entities/Monster.cpp $(CORE_DIR)/entities/Monster.h $(CORE_DIR)/entities/Character_sheet.h
	$(CC) $(CFLAGS) $< -o $@

obj/Siren.o: $(CORE_DIR)/entities/Siren.cpp $(CORE_DIR)/entities/Siren.h $(CORE_DIR)/entities/Monster.h
	$(CC) $(CFLAGS) $< -o $@

obj/Atlantean.o: $(CORE_DIR)/entities/Atlantean.cpp $(CORE_DIR)/entities/Atlantean.h $(CORE_DIR)/entities/Monster.h
	$(CC) $(CFLAGS) $< -o $@

obj/Nereid.o: $(CORE_DIR)/entities/Nereid.cpp $(CORE_DIR)/entities/Nereid.h $(CORE_DIR)/entities/Monster.h
	$(CC) $(CFLAGS) $< -o $@

###### Hero ######
obj/Hero.o: $(CORE_DIR)/entities/Hero.cpp $(CORE_DIR)/entities/Hero.h $(CORE_DIR)/entities/Character_sheet.h
	$(CC) $(CFLAGS) $< -o $@

###### Item ######
obj/Item.o: $(CORE_DIR)/entities/Item.cpp $(CORE_DIR)/entities/Item.h $(CORE_DIR)/entities/Entity.h $(CORE_DIR)/Cell.h
	$(CC) $(CFLAGS)  $< -o $@

################################ MAP / Cell ###############################
obj/Game.o: $(CORE_DIR)/map/Game.cpp $(CORE_DIR)/map/Game.h $(CORE_DIR)/map/Board_map.h
	$(CC) $(CFLAGS)  $< -o $@

obj/Board_map.o: $(CORE_DIR)/map/Board_map.cpp $(CORE_DIR)/map/Board_map.h $(CORE_DIR)/entities/Monster.h $(CORE_DIR)/entities/Hero.h $(CORE_DIR)/entities/Item.h $(CORE_DIR)/Position.h 
	$(CC) $(CFLAGS) $< -o $@

obj/Cell.o: $(CORE_DIR)/Cell.cpp $(CORE_DIR)/Cell.h $(CORE_DIR)/Position.h 
	$(CC) $(CFLAGS)  $< -o $@

obj/Position.o : $(CORE_DIR)/Position.cpp $(CORE_DIR)/Position.h 
	$(CC) $(CFLAGS)  $< -o $@

################################## DISPLAY ####################################
obj/Display.o: $(DISP_DIR)Display.cpp $(DISP_DIR)Display.h $(DISP_DIR)Image.h  $(CORE_DIR)/map/Board_map.h
	$(CC) $(CFLAGS) $(INCLUDE_DIR_SDL) $< -o $@

obj/Image.o: $(DISP_DIR)Image.cpp $(DISP_DIR)Image.h 
	$(CC) $(CFLAGS) $(INCLUDE_DIR_SDL) $< -o $@


###############################################################################
################################### OTHER #####################################
###############################################################################
.PHONY: clean
clean:
	rm -rf ./bin/[a-zA-Z0-9]* ./obj/[a-zA-Z0-9]* ./data/tmp* 

.PHONY: docs
docs: 
	@doxygen ./doc/image.doxy && firefox -new-window ./doc/html/index.html

.PHONY: debug
debug: all 
	@printf '\n\e[1;34m Executing GDB on the binaries... \e[0m \n\n'
	gdb -ex run ./bin/test -ex quit || gdb -ex start ./bin/test
	@printf '\n\e[1;34m Executing valgrind on the binaries... \e[0m \n\n'
	valgrind --tool=memcheck --suppressions=./doc/valgrind_lif7.supp --leak-check=full ./bin/test