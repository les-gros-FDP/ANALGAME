# Another_Nerdy_Abyssal_Light_Game_About_Yoga

A nice game that helps you stay calm





## Hero playble :
* You are a deep-depth diver in a *scaphandre*. The pipes and rope tying you to your ship were cut.
In order to survive you need to keep calm and concentrate, your reserve of oxygen are low but with yoga and meditation you find a way to consume so little oxygen that your air recycler can regenerate enough oxygen to continue.
The mission continue : find the temples deep down, they may contain the power for you to survive and reach the surface again.. or stay deep down forever..

### The temples :
* Temple of Poseidon : you'll find a trident and you can breath water. But now you have to defeat everyone who crave your power..
* Lost city of R'lyeh : you make a pact with the Old God Cthulhu. But now you have to meditate more to not be possessed forever, and defeat the Gods who want you gone...

### Note :
The trident can be throw and come back to you.
Cthulhu lets you harvest dark powers and tentacles protect you.

FR :
Le tour du joueur par default dure 30s * le pourcentage de vie restant. (donc si vie complète 30s, si il reste 1/3 de la vie 10s).
Il faut trouver une formule, du genre de base 30s puis 1s de plus par pdv en plus ? (faire un graph). si le temps est écoulé : pas d'actions possible, le tour est passé et le joueur perd 1pdv.
La meditation en combat permet de récupérer +100% de la vie actuelle (donc si 1/3 de la vie, ça fait + 1/3, si 2/5, ça fait +2/5) (ou alors seuleument +1pdv, à voir)
La méditation hors combat permet de récuperer toute la vie.
Il faut implémenter les moment "en combat" et en dehors des combats, pour les déplacements.